#!/usr/bin/env python
#
##  lurkerbot.py
#
#  Copyright 2014 Justin E. Kincaid <dotlineskribble@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
#   [18.09.2014 23:12:39 MST]

import irc.bot
import irc.strings
from irc.client import ip_numstr_to_quad, ip_quad_to_numstr, NickMask
from lurkerbotTwitter import lurkerbotTwitter
from lurkerbotTwitch import lurkerbotTwitch
from lurkerbotMath import lurkerbotMath
from lurkerbotGetCowSay import get_cowsay
from googlefluff import foogle_search
import praw
import random
import time
import requests
from os import path


class LurkerBot(irc.bot.SingleServerIRCBot):



    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel


        self.funcdict = {'!twitterrecent': self.grabstatus,
                    '!twitterrand': self.grabrandtweet,
                    '!killbot': self.killbot,
                    '!commands': self.listcommands,
                    '!doge': self.getdogeprice,
                    '!twitchers': self.onlinetwitchstreamers,
                    '!twitchbyname': self.twitchstreamersbyname,
                    '!twitchbygame': self.twitchstreamersbygame,
                    '!twitchbysearch': self.twitchstreamersbysearch,
                    '!math': self.searchwolfram,
                    '!moo': self.whats_thecow_say,
                    '!tits': self.getredditTITS,
                    '!fap': self.getredditFAP,
                    '!reddit': self.getredditARG,
                    '!google': self.getgoogleresults}

        self.userActivitylogpath = '~/bitbucket/python/lurkerBot/logs/lbUserActivity.log'

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.send_raw('auth LurkerBot TlstJL4MJu')
        c.mode(c.get_nickname(),'+x')
        time.sleep(0.55)

        c.join(self.channel)
        print c.get_server_name()
        for args in e.arguments:
            print args
        print 'joining ' + self.channel

    def on_topicinfo(self, c, e):

        print 'Topic on '+ e.arguments[0]+' set by '+e.arguments[1]+' on: \n' \
                + time.ctime(float(e.arguments[2]))
            #arg[0] = Channel
            #arg[1] = User who set topic
            #arg[2] = date in seconds since 1970-01-01 00:00:00 UTC


       # print e.source

    def on_currenttopic(self, c, e):

        print 'Topic for ' + e.arguments[0]+' is: ' + e.arguments[1]

    def on_part(self, c, e):

        print time.strftime("%d.%m.%Y[%H:%M:%S] "), e.source, 'left'

        with open(path.expanduser(self.userActivitylogpath), 'a') as userActivitylog:
            userActivitylog.write("{0} {1} {2}\n".format(time.strftime("%d.%m.%Y[%H:%M:%S] "), e.source, 'left'.rjust(8)))

    def on_quit(self, c, e):

        print time.strftime("%d.%m.%Y[%H:%M:%S] "), e.source, 'quit'
        with open(path.expanduser(self.userActivitylogpath ), 'a') as userActivitylog:

            userActivitylog.write("{0} {1} {2}\n".format(time.strftime("%d.%m.%Y[%H:%M:%S] "), e.source, 'quit'.rjust(8)))

    def on_join(self, c, e):

        for args in e.arguments:
            print args

        print time.strftime("%d.%m.%Y[%H:%M:%S] "), e.source, 'joined'

        with open(path.expanduser(self.userActivitylogpath ), 'a') as userActivitylog:

            userActivitylog.write("{0} {1} {2}\n".format(time.strftime("%d.%m.%Y[%H:%M:%S] "), e.source, 'joined'.rjust(8)))

        #c.topic(self.channel)
        # print 'joined'

    def on_privmsg(self, c, e):
        self.do_command(e, e.arguments[0])

    def on_pubmsg(self, c, e):

        chanmsg = e.arguments[0]
        channelobj = self.channels[self.channel]
        userNick = NickMask(e.source).nick

        #for usrs in channelobj.opers():
        #    print usrs
        if chanmsg[0] == '!':
            if channelobj.is_oper(userNick) or 'dot' in userNick:

                try:
                    spart = chanmsg.partition(' ')
                    command = spart[0].lower()
                    args = spart[2]
                    #print command
                    #print args
                    #check if string is empty assign None to args
                    if not args:
                       args = None
                       #print args
                except:
                    print 'no command or args found'

                if self.funcdict.has_key(command):

                    dofunc = self.funcdict[command]
                    if 'twitch' in command:
                        dofunc(e,args)
                    else:

                        dofunc(args)

                else:

                    c.privmsg(e.source, 'function not found')


            else:
                c.privmsg(NickMask(e.source).nick, 'must be op to command the bot')

        else:
            #print the msg to console
            self.printpubmsg_console(c,e)


        #if len(a) > 1 and irc.strings.lower(a[0]) == irc.strings.lower(self.connection.get_nickname()):

        #self.do_command(e, a[1].strip())
        return

    def on_dccmsg(self, c, e):
        c.privmsg("You said: " + e.arguments[0])

    def on_dccchat(self, c, e):
        if len(e.arguments) != 2:
            return
        args = e.arguments[1].split()
        if len(args) == 4:
            try:
                address = ip_numstr_to_quad(args[2])
                port = int(args[3])
            except ValueError:
                return
            self.dcc_connect(address, port)

    def do_command(self, e, cmd):
        nick = e.source.nick
        c = self.connection

        if cmd == "disconnect":
            self.disconnect()
        elif cmd == "die":
            self.die()
        elif cmd == "stats":
            for chname, chobj in self.channels.items():
                c.notice(nick, "--- Channel statistics ---")
                c.notice(nick, "Channel: " + chname)
                users = chobj.users()
                users.sort()
                c.notice(nick, "Users: " + ", ".join(users))
                opers = chobj.opers()
                opers.sort()
                c.notice(nick, "Opers: " + ", ".join(opers))
                voiced = chobj.voiced()
                voiced.sort()
                c.notice(nick, "Voiced: " + ", ".join(voiced))
        elif cmd == "dcc":
            dcc = self.dcc_listen()
            c.ctcp("DCC", nick, "CHAT chat %s %d" % (
                ip_quad_to_numstr(dcc.localaddress),
                dcc.localport))
        else:
            c.notice(nick, "Not understood: " + cmd)

    def grabstatus(self, user):

        twitterapi = lurkerbotTwitter()

        self.connection.privmsg(self.channel, twitterapi.statusgrab(user))

    def grabrandtweet(self, user):

        twitterapi = lurkerbotTwitter()

        self.connection.privmsg(self.channel, twitterapi.findrandomUserstatus(user))

    def killbot(self, args):

        self.die()

    def listcommands(self, args):

        commandstr = ""

        for key in sorted(self.funcdict.keys()):

            if len(commandstr.split(' '))%5==0:
                commandstr +=key+' \n'
            else:
                commandstr +=key+' '

        self.send_lines_to_chan(commandstr.splitlines())
        #print commandstr


    def getdogeprice(self, args):

        twitterapi = lurkerbotTwitter()

        self.send_lines_to_chan(twitterapi.finddogeprice().splitlines())

    def printpubmsg_console(self, c ,e):

        chanmsg = e.arguments[0]
        #find color for user name of msg sent for pyconsole output
        #unick=e.source.partition('!~')[0]
        unick=NickMask(e.source).user
        nicksum = sum(bytearray(unick.encode('UTF8')))
        nickcolor = nicksum%7 + 30
        print time.strftime("[%H:%M:%S] ")+'\33['+str(nickcolor)+'m'+ \
                unick.rjust(14,' ') +'\33[0m: '+chanmsg

    def onlinetwitchstreamers(self, e, args):

        twitchstream = lurkerbotTwitch(NickMask(e.source).user)
        self.send_lines_to_chan(twitchstream.liststreams())

    def twitchstreamersbygame(self, e, args):

        twitchstream = lurkerbotTwitch(NickMask(e.source).user)
        self.send_lines_to_chan(twitchstream.findstreamsbygame(args))

    def twitchstreamersbyname(self, e, args):

        twitchstream = lurkerbotTwitch(NickMask(e.source).user)
        self.connection.privmsg(self.channel, twitchstream.findstreamsbyname(args))

    def twitchstreamersbysearch(self, e, args):

        twitchstream = lurkerbotTwitch(NickMask(e.source).user)
        self.send_lines_to_chan(twitchstream.findstreamsbysearch(args))

    def searchwolfram(self, args):

        self.send_lines_to_chan(lurkerbotMath().findresult(args))

    #sends a list of strings sequentialy to current channel
    def send_lines_to_chan(self, list_of_strings):

        for string in list_of_strings:
            self.connection.privmsg(self.channel, string)
            time.sleep(0.355)

    def whats_thecow_say(self, arg):

        self.send_lines_to_chan(get_cowsay(arg))

    def getsubreddit(self, arg):

        returnReddit = []
        returnlimit = 5

        r = praw.Reddit('lurkerbot v0.01 dependency intensive irc bot by iydk')

        try:
            subreddit = r.get_subreddit(arg)
            returnReddit.append(subreddit._url)
            for sr in subreddit.get_hot(limit=returnlimit):
                returnReddit.append(str(sr)+" "+str(sr.url))
        except Exception as e:
            returnReddit.append(str(type(e))+" couldn't find subreddit")

        return returnReddit

    def getredditTITS(self, arg):

        self.send_lines_to_chan(self.getsubreddit('gonewild'))

    def getredditFAP(self, arg):

        self.send_lines_to_chan(self.getsubreddit('60fpsporn'))


    def getredditARG(self, arg):

        if arg is None:
            arg = 'random'

        self.send_lines_to_chan(self.getsubreddit(arg))

    def getgoogleresults(self, arg):

        self.send_lines_to_chan(foogle_search(arg))

def main():
    '''
    import sys
    if len(sys.argv) != 4:
        print("Usage: testbot <server[:port]> <channel> <nickname>")
        sys.exit(1)

    s = sys.argv[1].split(":", 1)
    server = s[0]
    if len(s) == 2:
        try:
            port = int(s[1])
        except ValueError:
            print("Error: Erroneous port.")
            sys.exit(1)
    else:
        port = 6667
    channel = sys.argv[2]
    nickname = sys.argv[3]
    '''
    channel = '#Slezoid'
    nickname = 'LurkerBot'
    server = 'irc.us.quakenet.org'
    port = 6667

    bot = LurkerBot(channel, nickname, server, port)
    bot.start()


if __name__ == "__main__":
    main()
