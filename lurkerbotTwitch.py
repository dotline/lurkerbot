#!/usr/bin/env python
#
##  lurkerbotTwitch.py
#
#  Copyright 2014 Justin E. Kincaid <dotlineskribble@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
#   [21.09.2014 01:25:08 MST


import requests
from os import path
import json


class lurkerbotTwitch():

    def __init__(self, user=None):

        baselurkerbotpath = '~/bitbucket/python/lurkerBot/data/twitch/'
        usersOAuthfile = 'usersOAuth.json'
        twitchGETHeaderfile = 'twitchGETHeader.json'

        self.usersOAuth = self.getjsonobj_fromfile(baselurkerbotpath+usersOAuthfile)
        if not user or user not in self.usersOAuth:
            user = '~dotline'
        self.twitchstreams = None
        self.headers = self.getjsonobj_fromfile(baselurkerbotpath+twitchGETHeaderfile)
        self.headers["Authorization"]+=self.usersOAuth[user]['access_token']
        url = 'https://api.twitch.tv/kraken/streams/followed'

        try:
            r = requests.get(url,headers=self.headers)
            r.raise_for_status()
            self.twitchstreams = r.json()['streams']

        except Exception as e:

            print type(e), r.status_code, 'request failed'
            self.twitchstreams = {"ERROR_RESPONSE":str(type(e))+" "+str(r.status_code)}

    #returns list of followed users as string of data
    def liststreams(self):

        followedTwitchers = []
        if "ERROR_RESPONSE" in self.twitchstreams:
            followedTwitchers.append(self.twitchstreams["ERROR_RESPONSE"])

        else:
            for i in xrange(len(self.twitchstreams)):

                tchannel=  self.twitchstreams[i]['channel']

                followedTwitchers.append(tchannel['display_name'] +' '+ tchannel['game']+' '+tchannel['url'])

        return followedTwitchers

    #returns string of followed twitcher if found else returns string not found
    def findstreamsbyname(self, twitcher):

        foundtwitcher = ""

        if "ERROR_RESPONSE" in self.twitchstreams:
            foundtwitcher = self.twitchstreams["ERROR_RESPONSE"]

        else:

            for i in xrange(len(self.twitchstreams)):

                tchannel=  self.twitchstreams[i]['channel']

                if twitcher.lower() in tchannel['display_name'].lower():

                    foundtwitcher = tchannel['display_name'] +' '+ tchannel['game']+' '+tchannel['url']

            if not foundtwitcher:
                foundtwitcher = 'twitcher not found'

        return foundtwitcher

    #returns list of streams if game found in followed streamer list
    def findstreamsbygame(self, game):

        followedTwitchers = []
        if "ERROR_RESPONSE" in self.twitchstreams:
            followedTwitchers.append(self.twitchstreams["ERROR_RESPONSE"])

        else:

            for i in xrange(len(self.twitchstreams)):

                tchannel=  self.twitchstreams[i]['channel']

                try:

                    if game.lower() in tchannel['game'].lower():
                        followedTwitchers.append(tchannel['display_name'] +' '+ tchannel['game']+' '+tchannel['url'])
                except:

                    print 'can not find any games matching title check oauth'

            if not followedTwitchers:
                followedTwitchers.append('no one you follow is streaming that game')
                for i in self.findstreamsbysearch(game):
                    followedTwitchers.append(i)


        return followedTwitchers

    #returns list of 3 top found searches
    def findstreamsbysearch(self, game):

        followedTwitchers = []

        try:

            searchstreams = None
            returnrange = 5
            params = {'q':game,'limit':25}
            url = 'https://api.twitch.tv/kraken/search/streams'
            r = requests.get(url,headers=self.headers,params=params)
            r.raise_for_status()
            searchstreams = r.json()['streams']
            followedTwitchers.append('I found some you might like')

            if returnrange >= len(searchstreams):
                returnrange = len(searchstreams)

            for i in xrange(returnrange):

                schannel = searchstreams[i]['channel']
                sviewers = searchstreams[i]['viewers']
                #print schannel['game'], sviewers
                followedTwitchers.append(schannel['display_name'] +' '+ schannel['game']+' '+schannel['url']+' viewers: '+str(sviewers))

        except Exception as e:

            followedTwitchers.append(str(type(e))+" "+r.status_code+' could not find any suggestions')

        return followedTwitchers

    def getjsonobj_fromfile(self, userPath):

        returnobj = None
        user_expanded_path = path.expanduser(userPath)
        if path.isfile(user_expanded_path):
            with open(user_expanded_path, 'r') as jsonfile:
                returnobj = json.load(jsonfile)

        return returnobj



'''
lbt = lurkerbotTwitch()
print 'STREAMSBYGAME:'
for u in sorted(lbt.findstreamsbygame('destiny')):
    print u

print '\n'

print 'STREAMBYNAME'
print lbt.findstreamsbyname('nadia')

print '\n'

print 'LISTSTREAMS'
for u in sorted(lbt.liststreams()):
    print u

game = 'hearthstone'

gametitle = 'Hearthstone: World of fartCraft'

if game.lower() in gametitle.lower():

    print game, gametitle

else:
    print game + 'not found'
'''
