#!/usr/bin/env python
#
##  untitled.py
#
#  Copyright 2014 Justin E. Kincaid <dotlineskribble@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
#   [04.10.2014 22:15:23 MST]

from commands import getoutput

#returns list of what the cowsays
def get_cowsay(arg):


    if arg is None:
        arg = 'mooooooo!'
    elif len(arg) > 143:
        arg = 'too long , moooooo!'

    cow_out = getoutput("cowsay "+arg)

    cow_out_list=[]

    for line in cow_out.splitlines():

  #      print line
        cow_out_list.append(line)


    return cow_out_list





#get_cowsay('fat mooing deal')
