#!/usr/bin/env python
#
##  untitled.py
#
#  Copyright 2014 Justin E. Kincaid <dotlineskribble@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
#   [07.10.2014 21:22:14 MST]

import requests
from bs4 import BeautifulSoup


def foogle_search(query):


    userquery = {'q':query,'safe':'off'}

    r = requests.get('https://www.google.com/search',params=userquery)

    print r.url
    #print r.headers
    #print r.content
    #print r.status_code
    soup = BeautifulSoup(r.text)
    gresults = soup.find(id="ires")

    list_results = []

    for atoms in gresults.find_all('h3'):

        list_results.append(atoms.get_text())
        urlresult = str.partition(str.partition(atoms.find('a').get('href'),'?q=')[2],'&')[0]
        list_results.append(urlresult)

    #print gresults.get_text()
    #return first 5 results
    #as list is {title,link,title,link,...}
    return list_results[:10]


'''
for answers in foogle_search('drones youtube'):

    print answers
'''
