#!/usr/bin/env python
#
##  lurkerbotTwitter.py
#
#  Copyright 2014 Justin E. Kincaid <dotlineskribble@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
#   [18.09.2014 19:45:25 MST]

import twitter
import random
import string
from os import path



#extends the twitter.Api initializes from text file in the format
#described below
class lurkerbotTwitter(twitter.Api):

    def __init__(self):

        try:
            '''
            format of twitter auth file should be as follows

                APIkey:yourapikeyhere
                APIsecret:yoursecretapikeyhere
                Accesstoken:youraccesstokenhere
                Accesstokensecret:youracesstokensecrethere

            no spaces and a new line for each key
            put your api key file in the 'apikeys' file located
            in base folder of your lurkerbot
            '''
            twitterAuth = open(path.expanduser('~/bitbucket/python/lurkerBot/apikeys/twitterauth.txt'), 'r')
            apikeys=[]
            for line in twitterAuth:

                title, key = line.strip('\n').split(':')
                apikeys.append(key)
               #print title , key       #debug statement

            consumer_key = apikeys[0]
            consumer_secret = apikeys[1]
            access_token_key = apikeys[2]
            access_token_secret = apikeys[3]

        except Exception:
            print Exception, 'Could not open or read auth file'

        try:
            twitter.Api.__init__(self,consumer_key=consumer_key,
                                 consumer_secret=consumer_secret,
                                 access_token_key=access_token_key,
                                 access_token_secret=access_token_secret)

        except Exception:

            print Exception , "your app could not authorize with twitter api"

    #Returns text of user params latest status
    #if no user is defined returns His Holiness' latest blessing
    def statusgrab(self, user=None):

        if user is None:
            user = 'dalailama'

        try:
            statuses = self.GetUserTimeline(screen_name=user)

            return_text = '\x032'+'@'+user+'\x0311'': '+statuses[0].text.replace('\n',' ')
        except Exception:

            return_text ="could not retrieve status"

        return return_text

    #Returns text from randon users status, if user param is None , else
    #returns random matching  user containing user params string
    def findrandomUserstatus(self, user=None):

        page = range(5)
        users = []

        if user is None:
            user = random.choice(string.ascii_lowercase)
            #print user

        try:
            for pages in page:

                users += self.GetUsersSearch(term=user,page=pages)

        except Exception:
            print Exception, "page number out of range"
            users = self.GetUsersSearch(term=user,page=1)

        try:
            ranuser = random.randrange(0, len(users)-1)
        except:

            ranuser = 0

        try:
            return_text = '\x032'+'@'+users[ranuser].screen_name+'\x0311'+': '+users[ranuser].GetStatus().text.replace('\n',' ')
        except Exception:

            return_text ="could not retrieve user status"

        return return_text


    def finddogeprice(self):

        user = 'priceofdogecoin'

        try:
            statuses = self.GetUserTimeline(screen_name=user)

            return_text = '\x032'+'@'+user+'\x0311'': \n'+statuses[0].text
        except Exception:

            return_text ="could not retrieve status"

        return return_text



api = lurkerbotTwitter()

#print api.VerifyCredentials()


print api.findrandomUserstatus()+'\n'

print api.findrandomUserstatus('miss')+'\n'

print api.findrandomUserstatus('science')+'\n'



print api.statusgrab('satan')+'\n'

print api.statusgrab()+'\n'

print api.statusgrab('iydkmightky')

print api.statusgrab('tascaz')



