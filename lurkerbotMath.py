#!/usr/bin/env python
#
##  untitled.py
#
#  Copyright 2014 Justin E. Kincaid <dotlineskribble@gmail.com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

#
#   [24.09.2014 21:30:53 MST]

import wolframalpha
import urllib

class lurkerbotMath():

    def __init__(self):

        try:
            app_id = "UWTUAH-PT5U5345XY"
            self.client = wolframalpha.Client(app_id)
        except Exception as e:

            print type(e), 'your id may be invalid'

    #returns list with result of query and link to webpage
    def findresult(self, args):

        list_results=[]
        try:
            results = self.client.query(args)

            answertext = next(results.results).text

            for line in answertext.splitlines():
                list_results.append(line)

        except Exception as e:
            print type(e), 'could not find answer'
            list_results.append('could not find answer')
            list_results.append("http://www.wolframalpha.com/input/?" + urllib.urlencode({'i':args}))
        #print(next(results.results).text)
        return list_results

#debug
#for i in lurkerbotMath().findresult('speed of light'):

#   print i


